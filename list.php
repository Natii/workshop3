<?php
    require("connection.php");
    $conexion = Connect();
    $sql = "SELECT * FROM category";
    $resultado = $conexion->query($sql);
    $categories = $resultado->fetch_all();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Workshop #3: PHP CRUD</title>
    <link rel="stylesheet" 
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
          crossorigin="anonymous">
</head>
<body>
    <div class = "container">
        <?php require ('header.php') ?>
        <h1>List of Categories</h1>
        <table class="table table-light">
            <tr>
                <th>ID</th>
                <th>Category Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            <tbody>
            <?php
                foreach($categories as $category) {
                    echo "<tr><td>".$category[0]."</td><td>".$category[1]."</td><td>".$category[2]."</td><td><a href=\"edit.php?id=".$category[0]."\">Edit</a> | <a href=\"delete.php?id=".$category[0]."\">Delete</a></tr>";
                }
            ?>
            </tbody>
        </table>
        <?php
            $conexion->close();
        ?>
    </div>
</body>
</html>