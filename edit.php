<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Workshop #3: PHP CRUD - Edit Category</title>
    <link rel="stylesheet" 
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
          crossorigin="anonymous">
</head>
<body>
<div class="container">
  <?php require ("header.php") ?>
    <h1>Category Edition</h1>
    <form action="edit.php" method="POST" class="form-inline" role="form">
      <div class="form-group">
        <label class="sr-only" for="">Name</label>
        <input type="text" class="form-control" name="name" placeholder="Category name">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Description</label>
        <input type="text" class="form-control" name="description" placeholder="Your description">
      </div>

      <input type="submit" class="btn btn-primary" value="Submit"></input>
    </form>
</div>
</body>
</html>

<?php
require("connection.php");
$conexion = Connect();
$id = $_GET["id"];
    if ($_POST) {
        $name = $_POST["name"];
        $description = $_POST["description"];
        $id = $_POST["id"];
        $sql = "UPDATE category SET name = '$name', description = '$description' WHERE id = '$id'";
        $resultado = mysqli_query($conexion, $sql);
        if ($resultado) {
            echo "Successfully created category";
        } else {
            echo "Error creating category";
        }
    } else {
        echo "Empty fields";
    }

mysqli_close($conexion);

?>